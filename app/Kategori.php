<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'kategori';

    public function article()
    {
		return $this->hasMany('App\Blog', 'id_kategori');
    }
}
