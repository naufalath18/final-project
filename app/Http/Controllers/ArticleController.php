<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use App\Blog;
use App\Kategori;

class ArticleController extends Controller
{
    public function index()
    {
        $data['blog'] =  Blog::get();
        $data['category'] =  Kategori::get();
		return view('home-article', $data);
    }

    public function detail($id)
    {
		$data['blog'] = Blog::find($id);

		return view('detail-article',  $data);
    }

    public function list()
    {
        return view('list-article');
    }

    public function my()
    {
        $data['blog'] =  Blog::get();
        $data['category'] =  Kategori::get();
        return view('my-article', $data);
    }

    public function addart()
    {
        $data['blog'] =  Blog::get();
        $data['category'] =  Kategori::get();
        return view('add-article', $data);
    }

    public function saveart(Request $request)
    {
        $save = new Blog();
        $save->judul            = $request->judul;
        $save->isi              = $request->isi;
        $save->id_kategori     = $request->id_kategori;
        $save->publish      = 0;
        $save->img_url      = $request->img_url;
        $save->save();
  
        return redirect('my-article');
    }

    public function editart(Request $request, $id)
    {
        $data['blog'] =  Blog::get();
        $data['category'] =  Kategori::get();
        
        $data['blog'] = Blog::find($id);

        return view('edit-article',  $data);
    }

    public function upart(Request $request, $id)
    {
        $update = Blog::find($id);
        $update->judul  = $request->judul;
        $update->isi    = $request->isi;
        $update->id_kategori = $request->id_kategori;
        $update->save();

        return redirect('my-article');
    }

    public function deleteart($id)
    {
        $hapus = Blog::where('id', $id)->first();
        $hapus->delete();
  
        return redirect('my-article');
    }
/*
    public function profil()
    {
        return view('profil');
    }*/

    public function profil($id)
    {
        $data['users'] = User::find($id);
        return view('profil', $data);
    }

    public function updateprofil(Request $request, $id)
    {
        $update = User::find($id);
        $update->name  = $request->name;
        $update->last_name    = $request->last_name;
        $update->gender = $request->gender;
        $update->tgl_lahir = $request->tgl_lahir;
        $update->alamat = $request->alamat;
        $update->email = $request->email;
        $update->save();

        return redirect()->back();
    }

    // public function editprofil($id)
    // {
    //     $data['users'] = User::find($id);
    //     return view('edit-profil', $data);
    // }
    
    public function categoryArticle(Request $request, $id)
    {
        $data['kategory'] = Kategori::find($id);
        return view('category-article', $data);
    }

    public function updateArticle(Request $request, $id)
    {
        $article = Blog::find($id);
        $article->judul = $request->judul;
        $article->isi = $request->isi;
        $article->id_kategori = $request->id_kategori;
        $article->save();

        return redirect()->back();
    }

}
