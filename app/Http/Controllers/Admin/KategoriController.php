<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Kategori;

class KategoriController extends Controller
{
    public function index(Request $request)
    {
    	$data['kategori'] = Kategori::get();
    	return view('admin/list-kategori', $data);
    }

    public function edit(Request $request, $id)
    {
        $data['kategori'] = Kategori::find($id);

        return view('admin.kategori-edit',  $data);
    }

    public function update(Request $request, $id)
    {
        $kategori = Kategori::find($id);
        $kategori->nama_kategori = $request->nama_kategori;
        $kategori->tipe = $request->tipe;
        $kategori->save();

        return redirect('admin/kategori');     
    }

    public function form()
    {
    	return view('admin/kategori-form');
    }

    public function addBlog(Request $request)
    {
    	$kategori = new Kategori;
		$kategori->nama_kategori = $request->nama_kategori;
		$kategori->tipe = $request->tipe;
		$kategori->save();

		return redirect('admin/kategori');
    }
}
