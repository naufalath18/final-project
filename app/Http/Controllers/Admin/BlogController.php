<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Blog;

class BlogController extends Controller
{
    public function index(Request $request)
    {
    	$data['blog'] = Blog::get();
    	return view('admin/list-blog', $data);
    }

    public function edit(Request $request, $id)
    {
		$data['blog'] = Blog::find($id);
		return view('admin.blog-edit',  $data);
    }

    public function update(Request $request, $id)
    {
    	$blog = Blog::find($id);
    	$blog->judul = $request->judul;
    	$blog->isi = $request->isi;
    	$blog->publish = $request->publish;
    	$blog->save();

    	return redirect('admin/blog');
    }
}
