<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Validator;
use DB;

class UsersController extends Controller
{
    public function index(Request $request)
    {
    	$data['user'] = User::get();
    	return view('admin/users', $data);
    }

    public function edit(Request $request, $id)
    {
		$data['user'] = User::find($id);

		return view('admin.user-edit',  $data);
    }

    public function update(Request $request, $id)
    {
    	$user = User::find($id);
		$user->name = $request->name;
		$user->last_name = $request->last_name;
		$user->gender = $request->gender;
		$user->tgl_lahir = $request->tgl_lahir;
		$user->alamat = $request->alamat;
		$user->level = 1;
		$user->email = $request->email;
    	$user->save();

		return redirect('admin/users');    	
    }

    public function form()
    {
    	return view('admin/user-form');
    }

    public function addUser(Request $request)
    {
    	$rules = array(
        'password'         => 'required',
        'password_confirm' => 'required|same:password'
    	
    		);

    	$validator = Validator::make($request->all(), $rules);

		$user = new User;
		$user->name = $request->name;
		$user->last_name = $request->last_name;
		$user->gender = $request->gender;
		$user->tgl_lahir = $request->tgl_lahir;
		$user->alamat = $request->alamat;
		$user->level = 1;
		$user->email = $request->email;
        $user->password = $request->password;
		$user->save();

		return redirect('admin/users');
    }

    public function destroy($id)
    {
        DB::table('users')->where('id',$id)->delete();
        return redirect('admin/users');
    }

    public function destroyKomen($id)
    {
        DB::table('komentar')->where('id',$id)->delete();
        return redirect('admin/komentar');
    }
}
