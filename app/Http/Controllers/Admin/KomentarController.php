<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Komentar;

class KomentarController extends Controller
{
	public function index(Request $request)
	{
	    $data['komentar'] = Komentar::get();
	    return view('admin/list-komentar', $data);
	}
}
