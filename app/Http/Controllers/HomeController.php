<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('/');
    }

    public function afterLogin(Request $request)
    {
        // cek role
        if ($request->user()->level == 1) {
            return redirect('admin/dashboard');
        }else{
            return redirect('/');
        }
    }
}
