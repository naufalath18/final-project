<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('after-login', 'HomeController@afterLogin');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'ArticleController@index')->name('list-article');
Route::get('/detail-article/{id}', 'ArticleController@detail')->name('detail-article');
Route::get('/list-article', 'ArticleController@list')->name('list-article');
Route::get('/my-article', 'ArticleController@my');
Route::get('/add-article', 'ArticleController@addart');
Route::post('/save-article', 'ArticleController@saveart');
Route::get('/edit-article/{id}', 'ArticleController@editart');
Route::post('/edit-article/{id}', 'ArticleController@updateArticle');
Route::post('/update-article/{id}', 'ArticleController@upart');
Route::get('/delete-article/{id}', 'ArticleController@deleteart');


Route::get('/contact', 'ArticleController@contact')->name('contact');

Route::get('/profil/{id}', 'ArticleController@profil')->name('profil');
Route::post('/profil/{id}', 'ArticleController@updateprofil');

Route::get('/article/category/{id}', 'ArticleController@categoryArticle');




// Space admin
Route::get('admin/dashboard', 'Admin\DashboardController@dashboard');
Route::get('admin/users', 'Admin\UsersController@index');
Route::get('admin/user/form', 'Admin\UsersController@form');
Route::post('admin/user/form', 'Admin\UsersController@addUser');
Route::get('admin/user/{id}', 'Admin\UsersController@edit');
Route::post('admin/user/{id}', 'Admin\UsersController@update');
Route::delete('admin/user/{id}', 'Admin\UsersController@destroy');

Route::get('admin/blog', 'Admin\BlogController@index');
Route::get('admin/blog/{id}', 'Admin\BlogController@edit');
Route::post('admin/blog/{id}', 'Admin\BlogController@update');

// Route::get('admin/blog/form', 'Admin\BlogController@form');
// Route::post('admin/blog/form', 'Admin\BlogController@addBlog');

Route::get('admin/kategori', 'Admin\KategoriController@index');
Route::get('admin/kategori/form', 'Admin\KategoriController@form');
Route::post('admin/kategori/form', 'Admin\KategoriController@addBlog');
Route::get('admin/kategori/{id}', 'Admin\KategoriController@edit');
Route::post('admin/kategori/{id}', 'Admin\KategoriController@update');

Route::get('admin/komentar', 'Admin\KomentarController@index');
Route::delete('admin/komentar/{id}', 'Admin\UsersController@destroyKomen');
// Route::get('admin/komentar/form', 'Admin\KomentarController@form');
// Route::post('admin/komentar/form', 'Admin\KomentarController@addUser');
// Route::get('admin/komentar/{id}', 'Admin\KomentarController@edit');
// Route::post('admin/komentar/{id}', 'Admin\KomentarController@update');