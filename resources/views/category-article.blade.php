@extends('layouts.app')

@section('content')
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div>
            <h1>{{ $kategory->nama_kategori }}</h1>
        </div>
    </nav>
     <div class="row justify-content-center mt-3">
        <div class="card-deck">
                @foreach($kategory->article as $row)
            <div class="card">
                <img class="card-img-top" src="{{asset('img/thumb-example.svg')}}" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">{{ $row->judul }}</h5>
                    <!-- <p class="card-text">{{ substr($row->isi, 10) }}</p> -->
                    <p class="card-text"><small class="text-muted">{{ $row->created_at }}</small></p>
                    <a href="{{  url('/detail-article/'.$row->id) }}" class="btn btn-primary">Read more</a>
                </div>
            </div>
                @endforeach
        </div>
    </div>
</div>
@endsection 