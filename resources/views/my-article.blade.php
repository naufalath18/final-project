@extends('layouts.app')

@section('content')
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ url('/') }}">Home <span class="sr-only">(current)</span></a>
                </li>
                @foreach($category as $row)
                <li class="nav-item">
                    <a class="nav-link" href="{{  url('article/category/'.$row->id) }}">{{ $row->nama_kategori }}</a>
                </li>
                @endforeach
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
    </nav>
    <h1 class="my-4">My Article</h1>
    <a href="{{ url('/add-article') }}" class="btn btn-success">+</a>
    <label><h5>Tambahkan Artikel</h5></label>
    <table class="table mt-4">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Judul</th>
      <th scope="col">Dibuat pada</th>
      <th scope="col">Diubah pada</th>
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>
  	@foreach($blog as $key => $row)
    <tr>
      <th scope="row">{{ $key+1 }}</th>
      <td>{{ $row->judul}}</td>
      <td>{{ $row->created_at}}</td>
      <td>{{ $row->updated_at}}</td>
      <td>
      	<a href="{{ url('/edit-article') }}/{{ $row->id }}"><button type="button" class="btn btn-primary">Ubah</button></a>
        <a href="{{ url('/delete-article') }}/{{ $row->id }}"><button type="button" class="btn btn-danger">Hapus</button></a>
       </td>
    </tr>
      @endforeach
  </tbody>
</table>
    @endsection