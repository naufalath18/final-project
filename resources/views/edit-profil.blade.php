@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Selamat datang di halaman pribadi anda</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Masukan yang ingin diubah</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                       <form method="POST">
                        @csrf
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" value="{{ $users->name }}">
                        </div>
                        <div class="form-group">
                            <label>Nama belakang</label>
                            <input type="text" name="last_name" class="form-control" value="{{ $users->last_name }}" >
                        </div>
                        <div class="form-group">
                            <label for="gender">Gender</label>
                            <select name="gender" class="form-control">
                            <option value="{{ $users->gender }}" selected>{{ $users->gender }}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Tanggal Lahir</label>
                            <input type="date" name="tgl_lahir" class="form-control" value="{{ $users->tgl_lahir }}">
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <textarea name="alamat" value="" class="form-control" >{{ $users->alamat }}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control" value="{{ $users->email }}">
                        </div>
                        <div class="form-group mt-5">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                           
                       </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
@endsection