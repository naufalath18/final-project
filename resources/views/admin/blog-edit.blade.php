@extends('layouts.admin_master')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Approval Content</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Pilih aksi</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                       <form method="POST">
                        @csrf
                        <div class="form-group">
                            <label>Judul</label>
                            <input type="text" name="judul" class="form-control" value="{{ $blog->judul }}" readonly="">
                        </div>
                        <div class="form-group">
                            <label>Isi</label>
                            <textarea class="form-control" readonly="" value="" name="isi">{{ $blog->isi }}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Aksi</label>
                            <select class="form-control" value="{{ $blog->publish }}" name="publish">
                                <option value="0">Hold Publish</option>
                                <option value="1">Publish</option>
                            </select>
                        </div>
                        <div class="form-group mt-5">
                            <button class="btn btn-success">Simpan</button>
                        </div>
                           
                       </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
@endsection