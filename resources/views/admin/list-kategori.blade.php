@extends('layouts.admin_master')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Kategori</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Tambah Kategori -> <a href="{{ url('admin/kategori/form') }}">New</a></h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Kategori</th>
                                   {{--  <th>Tipe</th> --}}
                                    <th>Created at</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $no = 1; @endphp
                                @foreach($kategori as $key => $row)
                                <tr>
                                   <td>{{ $no++ }}</td>
                                   <td>{{ $row->nama_kategori }}</td>
                                   {{-- <td>{{ $row->tipe }}</td> --}}
                                   <td>{{ $row->created_at }}</td>
                                   <td>
                                       <a href="{{ url('admin/kategori/'.$row->id) }}" class="btn btn-primary">Edit</a>
                                   </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
@endsection