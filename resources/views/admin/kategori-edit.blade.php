@extends('layouts.admin_master')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edit Kategori</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Masukan data yang ingin diubah</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                       <form method="POST">
                        @csrf
                        <div class="form-group">
                            <label>Nama Kategori</label>
                            <select class="form-control" name="nama_kategori" value="">
                                <option value="{{ $kategori->nama_kategori }}" selected>{{ $kategori->nama_kategori }}</option>
                                <option value="Hewan">Hewan</option>
                                <option value="Tanaman">Tanaman</option>
                                <option value="Otomotif">Otomotif</option>
                                <option value="Elektronik">Elektronik</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Tipe</label>
                            <input type="text" name="tipe" class="form-control" value="{{ $kategori->tipe }}">
                        </div>
                        <div class="form-group mt-5">
                            <button class="btn btn-success">Simpan</button>
                        </div>
                           
                       </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
@endsection