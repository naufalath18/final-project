@extends('layouts.app')

@section('content')
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ url('/') }}">Home <span class="sr-only">(current)</span></a>
                </li>
                @foreach($category as $row)
                <li class="nav-item">
                    <a class="nav-link" href="{{  url('article/category/'.$row->id) }}">{{ $row->nama_kategori }}</a>
                </li>
                @endforeach
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
    </nav>
    <div class="row justify-content-center mt-4">
        <div class="card-deck">
                @foreach($blog as $row)
            <div class="card">
                <img class="card-img-top" src="{{asset('img/thumb-example.svg')}}" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">{{ $row->judul }}</h5>
                    <!-- <p class="card-text">{{ substr($row->isi, 10) }}</p> -->
                    <p class="card-text"><small class="text-muted">{{ $row->created_at }}</small></p>
                    <a href="{{  url('/detail-article/'.$row->id) }}" class="btn btn-primary">Read more</a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection