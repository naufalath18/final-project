@extends('layouts.app')

@section('style')
<script type="text/javascript" src="{{ asset('plugin/ckeditor/ckeditor.js')}}"></script>
@endsection

@section('content')
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ url('/') }}">Home <span class="sr-only">(current)</span></a>
                </li>
                @foreach($category as $row)
                <li class="nav-item">
                    <a class="nav-link" href="{{  url('article/category/'.$row->id) }}">{{ $row->nama_kategori }}</a>
                </li>
                @endforeach
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
    </nav>
    <div class="jumbotron">
        <form method="post">
            @csrf
            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputEmail">Judul</label>
                    <input type="text" class="form-control" name="judul" id="exampleInputEmail1" value="{{ $blog->judul }}">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Masukkan Isi Artikel</label>
                    <textarea class="ckeditor" name="isi" id="exampleFormControlTextarea1" rows="3">{{ $blog->isi }}</textarea>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Pilih Kategori</label>
                    <select class="form-control" name="id_kategori" id="exampleFormControlSelect1">
                        <option value="">--Pilih--</option>
                        @foreach ($category as $key => $row)
                        <option value="{{ $row->id }}" {{ ($blog->id_kategori == $row->id) ? 'selected' : '' }}>{{ $row->nama_kategori }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            {{-- <div class="form-group">
                <label for="exampleFormControlFile1">Example file input</label>
                <input type="file" name="img_url" class="form-control-file" id="exampleFormControlFile1">
            </div> --}}
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
        
    </div>
</div>
@endsection 