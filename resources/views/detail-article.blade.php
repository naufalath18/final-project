@extends('layouts.app')

@section('content')
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
<!-- 
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ url('/') }}">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{  url('/list-article') }}">Article</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{  url('/about') }}">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{  url('/contact') }}">Contact Us</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Dropdown
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div> -->
    </nav>
    <!-- <div class="row justify-content-center mt-3"> -->
       {{--  <div class="card-deck">
            <div class="card">
                <img class="card-img-top" src="{{asset('img/thumb-example.svg')}}" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">{{ $blog->judul }}</h5>
                    <p class="card-text">{{ $blog->isi }}</p>
                    <p class="card-text"><small class="text-muted">{{ $blog->created_at }}</small></p>
                </div>
            </div>
        </div> --}}
    <!-- </div> -->
    <div class="jumbotron">
         <img class="" style="width: 100%" src="{{asset('img/thumb-example.svg')}}" alt="Card image cap">
         <h1 class="mt-3">{{ $blog->judul }}</h1>
         <small><i>{{ $blog->category->nama_kategori}}, Diposting pada {{ $blog->created_at->diffForHumans() }}</i></small>

         <div class="mt-3">
            {!! $blog->isi !!}
         </div>
        
    </div>
</div>
@endsection